import React, { memo } from "react";
import theme from "../../assets/theme";
import {
    MainView,
    Label,
    TextField
} from "./style"
const TextArea = (props) => {

    return <MainView>
        {props.label ? <Label
            color={theme.default}
            fontSize={10}
        >{props.label}</Label> : <></>}
        <TextField
            textAlignVertical={'top'}
            placeholder={props.placeholder}
            onChangeText={props.handleChange}
            value={props.value}
        />
    </MainView>
}

TextArea.defaultProps = {
    value: '',
    handleChange: () => { },
    label: '',
    placeholder: ''
}

export default memo(TextArea, (prevProps, nextProps) => {
    if (prevProps.value === nextProps.value) {
        return true;
    }
    return false;
})