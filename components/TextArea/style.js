import styled from 'styled-components/native';
import TextComponent from '../Text';

export const MainView = styled.View`
  width: 100%;
  display: flex;
  flex-direction: column
`;

export const Label = styled(TextComponent)`
  margin-bottom: 10px
`

export const TextField = styled.TextInput`
  height: 120px;
  border-color: #999;
  border-radius: 15px;
  border-width: 1px;
  padding: 10px
`