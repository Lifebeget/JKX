import React, { useCallback, useMemo } from "react";
import DocumentPicker from "react-native-document-picker";
import { CONSTANTS } from "../../actions/constants";
import { useToast } from "react-native-toast-notifications";
import { MainView, UploadText } from "./style";
import Icon from "../../assets/svg/upload";
import theme from "../../assets/theme";
import { useSelector } from 'react-redux';
import { Platform } from "react-native";
import FileSize, { InvalidUnit } from "../../containers/Feedback/FileItem/converterFile";

const Upload = (props) => {
  const toast = useToast();
  const { width } = useSelector(state => state.App.Dimensions);

  const pickFile = useCallback(async () => {
    try {
      const file = await DocumentPicker.pickSingle({
        type: [...CONSTANTS.file_valid_extension]
      });
      const fileType = file.type;
      if (fileType) {
        const realURI = Platform.select({
          android: file.uri,
          ios: decodeURI(file.uri),
        });
        if (realURI) {
          const [size, unit] = FileSize(file?.size)?.split(' ');
          if (InvalidUnit.find(item => item === unit)) {
            throw new Error('Файл не должен быть больше 7МБ')
            return
          }
          if (unit === 'МБ') {
            if (+size >= 7) {
              throw new Error('Файл не должен быть больше 7МБ')
              return
            }
          }
          props.handleChange([{
            ...file
          }])
        }
      }
    } catch (e) {
      if (DocumentPicker.isCancel(e)) {
        props.handleChange([])
      } else {
        props.handleChange([])
        toast.show(e.message, {
          type: "danger",
          placement: "bottom",
          duration: 4000,
          offset: 30,
          animationType: "slide-in",
        })
      }
    }
  }, []);

  const maxWidth = useMemo(() => {
    return width / 2.5
  }, [width])

  return <MainView
    disabled={props.disabled}
    activeOpacity={0.5}
    onPress={pickFile}
    maxWidth={maxWidth}
  >
    <Icon color={props.disabled ? theme.gray : theme.focused} />
    <UploadText fontSize={10} color={props.disabled ? theme.gray : theme.focused}>Прикрепить файл</UploadText>
  </MainView>
}

export default Upload