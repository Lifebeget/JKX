import styled from 'styled-components/native';
import TextComponent from '../Text';

export const MainView = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  max-width: ${(props) => props.maxWidth}px;
`;

export const UploadText = styled(TextComponent)`
 text-transform: uppercase;
 margin-left: 10px;
 margin-top: 3px;
 margin-bottom: 3px
`