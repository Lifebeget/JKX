import styled from 'styled-components/native';

export const BackgroundView = styled.ImageBackground`
  width: 100%;
  height: 100%;
  flex: 1
`

export const SafeView = styled.SafeAreaView`
  flex: 1
`

export const KeyboardView = styled.KeyboardAvoidingView`
 flex: 1
`

export const ScrollViews = styled.ScrollView`
  flex: 1;
  height: 100%
`

export const Container = styled.View`
  flex: 1;
  width: 100%;
  padding-left: 40px;
  padding-right: 40px;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column
`;

export const TextView = styled.View`
 width: 90%;
 display: flex;
 align-items: center;
 justify-content: center;
`

export const Block = styled.View`
  flex: 1;
  background-color: rgba(252, 254, 255, 0.8);
  align-items: center;
  display: flex;
  border-radius: 10px;
  width: 100%;
  overflow: hidden
`;


export const UnionBlock = styled.View`
  width: 100%;
  margin-left: 2%;
  margin-bottom: 1%
`

export const TextFieldBlock = styled.View`
 margin-top: ${(props) => props.mt ? props.mt : 20}px
`;

export const BottomBlock = styled.View`
  width: 90%
`

export const ButtonBlock = styled.View`
 width: 100%;
 display: flex;
 flex-direction: row;
 align-items: center;
 justify-content: space-between;
 margin-top: 15px
`;

export const ButtonView = styled.View`
max-width: 91px;
min-width: 62px
`

export const Divider = styled.View`
 width: ${(props) => props.h}
`

export const CheckboxView = styled.View`
  display: flex;
  flex-direction: row;
  width: ${(props) => {
        return props.fullWidth
    }}px;
  justify-content: flex-start;
  margin-left: 20px
`;

export const ForgotView = styled.View`
 width: 100%;
 margin-top: 10px;
 margin-bottom: 10px;
`;

export const BottomLine = styled.View`
  width: 100%;
  border-bottom-color: rgba(70, 65, 65, 0.7);
  border-bottom-width: 0.5px;
  marginBottom: 10px 
`

export const ContainerChild = styled.View`
 width: 90%
`