import React, { useEffect, useState } from "react";
import { View, TouchableOpacity } from "react-native";
import messaging from "@react-native-firebase/messaging";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useDispatch, useSelector } from "react-redux";
import Union from "../../assets/svg/union";
import Warning from "../../assets/svg/warning";
import Header from "../../components/Header";
import Text from "../../components/Text";
import TextField from "../../components/TextField";
import Button from "../../components/Button";
import Colors from "../../assets/theme";
import Checkbox from "../../components/Checkbox";
import {
  fetchActiveAccount,
  fetchCheck,
  fetchLogin,
  fetchRooms,
  sendToken,
} from "../../actions/fetch";
import { setAccount, setRooms } from "../../redux/reducers/Main";
import {
  BackgroundView,
  SafeView,
  KeyboardView,
  ScrollViews,
  Container,
  TextView,
  Block,
  UnionBlock,
  TextFieldBlock,
  BottomBlock,
  ButtonBlock,
  ButtonView,
  Divider,
  CheckboxView,
  ForgotView,
  BottomLine,
  ContainerChild,
} from "./style";

const Component = () => {
  const dispatch = useDispatch();
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [save, setSave] = useState(false);
  const [loading, setLoading] = useState(false);
  const activeRoom = useSelector((state) => state.Main.activeRoom);
  const [error, setError] = useState({
    login: false,
    password: false,
  });
  const [errors, setErrors] = useState(false);
  const [blockWidth, setBlockWidth] = useState(0);

  useEffect(() => {
    AsyncStorage.getItem("LogPass").then((data) => {
      try {
        setLogin(JSON.parse(data).login);
        setPassword(JSON.parse(data).password);
      } catch (e) {
        setLogin("");
        setPassword("");
      }
    });
  }, []);
  const requestPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      return !!authStatus;
    } else {
      return false;
    }
  };
  const getFcmToken = async () => {
    const status = await requestPermission();
    if (status) {
      return await messaging().getToken();
    } else {
      return null;
    }
  };
  const getActive = async () => {
    try {
      const rooms = await fetchRooms();
      await AsyncStorage.setItem("rooms", JSON.stringify(rooms));
      dispatch(setRooms(rooms));
      const account = await fetchActiveAccount();
      await AsyncStorage.setItem("account", JSON.stringify(account));
      dispatch(setAccount(account));
      const check = await fetchCheck();
      await AsyncStorage.setItem("check", String(check.allowed));
      dispatch({ type: "SET_CHECK", payload: check.allowed });
      if (rooms[activeRoom]) {
        if (rooms[activeRoom].company) {
          dispatch({
            type: "SET_ROOM_COMPANY",
            payload: rooms[activeRoom].company,
          });
        }
      }
      const token = await getFcmToken();
      const sending = await sendToken(token);
      return !!sending;
    } catch (e) {
      return false;
    }
  };
  const Login = () => {
    dispatch({ type: "SETISAUTH", payload: false });
    dispatch({ type: "RESET_STORE" });
    setError({
      login: !login.trim().length,
      password: !password.trim().length,
    });
    if (login.trim() && password.trim()) {
      setLoading(true);
      fetchLogin({
        login: login,
        password: password,
      }).then((data) => {
        if (data) {
          if (data.isAuth) {
            if (save) {
              AsyncStorage.setItem(
                "LogPass",
                JSON.stringify({
                  login: login,
                  password: password,
                })
              ).then(() => {
                getActive().then((status) => {
                  if (status) {
                    dispatch({ type: "LOGIN", payload: data });
                  }
                });
                setErrors(false);
              });
            } else {
              getActive().then((status) => {
                if (status) {
                  dispatch({ type: "LOGIN", payload: data });
                }
              });
              setErrors(false);
            }
          } else {
            setErrors(true);
          }
        } else {
          setErrors(true);
        }
        setLoading(false);
      });
    }
  };
  return (
    <BackgroundView source={require("../../assets/img/login-bg.webp")}>
      <Header />
      <SafeView>
        <KeyboardView>
          <ScrollViews>
            <Container>
              <TextView>
                <Text color={Colors.white} fontSize={16} fontWeight="700">
                  {"\n"}
                  Многофункциональная
                  {"\n"}
                  СRМ/ERP-система для
                  {"\n"}
                  управляющих организаций ЖКХ
                  {"\n"}
                </Text>
              </TextView>
              <Block>
                <UnionBlock>
                  <Union />
                </UnionBlock>
                <ContainerChild>
                  <View
                    onLayout={(event) => {
                      try {
                        setBlockWidth(event.nativeEvent.layout.width - 91);
                      } catch (_) {}
                    }}
                  >
                    <TextFieldBlock>
                      <TextField
                        testID="login"
                        accessibilityLabel="login"
                        Value={login}
                        onChange={(value) => setLogin(value)}
                        error={login ? false : error.login}
                        helperText={
                          login
                            ? null
                            : error.login
                            ? "Это поле обязательно для заполнения"
                            : null
                        }
                        label="Логин"
                      />
                    </TextFieldBlock>
                    <TextFieldBlock mt={30}>
                      <TextField
                        testID="password"
                        accessibilityLabel="password"
                        Value={password}
                        onChange={(value) => setPassword(value)}
                        error={password ? false : error.password}
                        helperText={
                          password
                            ? null
                            : error.password
                            ? "Это поле обязательно для заполнения"
                            : null
                        }
                        type="password"
                        label="Пароль"
                      />
                    </TextFieldBlock>
                    <BottomBlock>
                      <ButtonBlock>
                        <ButtonView>
                          <Button
                            onPress={Login}
                            borderRadius
                            line
                            logo
                            isLoading={loading}
                            disabled={loading}
                            color="primary"
                          >
                            <Text
                              fontSize={10}
                              color={Colors.white}
                              fontWeight="normal"
                            >
                              {loading ? "Загрузка" : "Войти"}
                            </Text>
                          </Button>
                        </ButtonView>
                        <CheckboxView fullWidth={blockWidth}>
                          <Checkbox
                            checked={save}
                            color={Colors.primary}
                            onPress={() => setSave(!save)}
                          />
                          <Divider h={5} />
                          <TouchableOpacity onPress={() => setSave(!save)}>
                            <Text
                              style={{ marginLeft: "2%" }}
                              color={Colors.black}
                              fontSize={9.5}
                              numberOfLines={2}
                              fontWeight="300"
                            >
                              Сохранить
                            </Text>
                          </TouchableOpacity>
                        </CheckboxView>
                      </ButtonBlock>
                      <ForgotView>
                        <TouchableOpacity
                          onPress={() => {
                            console.log("forgot");
                          }}
                        >
                          <Text
                            color={Colors.black}
                            fontSize={8}
                            fontWeight="300"
                          >
                            Забыли пароль?
                          </Text>
                        </TouchableOpacity>
                      </ForgotView>
                    </BottomBlock>
                  </View>
                </ContainerChild>
                <BottomLine />
                {errors ? (
                  <View
                    style={{
                      width: "90%",
                      maxHeight: 80,
                      marginBottom: 10,
                      borderWidth: 1,
                      borderColor: Colors.white,
                    }}
                  >
                    <View
                      style={{
                        width: "100%",
                        backgroundColor: "rgba( 207, 207, 205, 0.2)",
                        height: "100%",
                        flexDirection: "row",
                      }}
                    >
                      <View
                        style={{
                          width: "20%",
                          height: "100%",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <View
                          style={{
                            width: "100%",
                            height: "50%",
                            opacity: 0.5,
                          }}
                        >
                          <Warning />
                        </View>
                      </View>
                      <View
                        style={{
                          width: "80%",
                          height: "100%",
                          padding: "5%",
                        }}
                      >
                        <Text
                          style={{ textTransform: "uppercase" }}
                          fontWeight="bold"
                          color={Colors.error}
                          fontSize={9}
                        >
                          Не указаны параметры для входа в систему
                        </Text>
                      </View>
                    </View>
                  </View>
                ) : null}
              </Block>
            </Container>
          </ScrollViews>
        </KeyboardView>
      </SafeView>
    </BackgroundView>
  );
};
export default Component;
