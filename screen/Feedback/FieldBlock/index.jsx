import React, { memo, useCallback, useEffect, useState } from "react"
import { MainBlock, AttachButton, Block, TextField, ButtonView, ButtonComponent } from "./style";
import Attachment from "../../../assets/svg/attachment";
import theme from "../../../assets/theme";
import { Platform } from "react-native";
import DocumentPicker from "react-native-document-picker";
import { useToast } from "react-native-toast-notifications";
import { CONSTANTS } from "../../../actions/constants";
import FileSize, { InvalidUnit } from "../FileItem/converterFile";
import { ImageListBlock } from "../style";
import FileItem from "../FileItem";
import SendIcon from "../../../assets/svg/send";

const FieldBlock = (props) => {
    const [files, setFiles] = useState([]);
    const [value, setValue] = useState('');
    const [loading, setLoading] = useState(false);

    const toast = useToast();

    const handleDelete = (i) => {
        setFiles(oldFile => {
            let f = [...oldFile];
            f.splice(i, 1);
            return [...f]
        })
    }

    const pickFile = useCallback(async () => {
        try {
            const file = await DocumentPicker.pickSingle({
                type: [...CONSTANTS.file_valid_extension]
            });
            const fileType = file.type;
            if (fileType) {
                const realURI = Platform.select({
                    android: file.uri,
                    ios: decodeURI(file.uri),
                });
                if (realURI) {
                    const [size, unit] = FileSize(file?.size)?.split(' ');
                    if (InvalidUnit.find(item => item === unit)) {
                        throw new Error('Файл не должен быть больше 7МБ')
                        return
                    }
                    if (unit === 'МБ') {
                        if (+size >= 7) {
                            throw new Error('Файл не должен быть больше 7МБ')
                            return
                        }
                    }
                    if (files.length !== 5) {
                        setFiles(oldFile => {
                            const f = [...oldFile]
                            f.push({
                                ...file
                            })
                            return f
                        })
                    }
                }
            }
        } catch (e) {
            if (!DocumentPicker.isCancel(e)) {
                toast.show(e.message, {
                    type: "danger",
                    placement: "bottom",
                    duration: 4000,
                    offset: 30,
                    animationType: "slide-in",
                })
            }
        }
    }, []);

    const onSend = useCallback(() => {
        setLoading(true)
        props.handleSend(value, files).then(() =>{
            setLoading(false);
            setFiles([]);
            setValue('')
        });
    }, [value, files])

    return <MainBlock>
        {
            files.length ? <ImageListBlock horizontal={true}>
                {files.map((item, index) => {
                    return <FileItem onDelete={handleDelete} end={files.length - 1} index={index} key={index} {...item} />
                })}
            </ImageListBlock> : <></>
        }
        <Block>
            <AttachButton disabled={files.length === 5} activeOpacity={0.7} onPress={pickFile}>
                <Attachment color={files.length === 5 ? theme.gray : theme.focused} />
            </AttachButton>
            <TextField
                defaultValue={''}
                value={value}
                placeholder="Ваше сообщение..."
                onChangeText={setValue}
            />
            <ButtonView>
                <ButtonComponent
                    disabled={!value?.length || loading}
                    icon={SendIcon}
                    logo
                    onPress={onSend}
                    borderRadius="15px"
                />
            </ButtonView>
        </Block>
    </MainBlock>
}

export default memo(FieldBlock, (prevProps, nextProps) => {
    if (JSON.stringify(prevProps) === JSON.stringify(nextProps)) {
        return true
    }
    return false
})