import styled from 'styled-components/native';
import Button from "../../../components/Button";

export const MainBlock = styled.View`
 width: 100%;
 margin-top: 5px;
`;

export const AttachButton = styled.TouchableOpacity`
 margin-right: 5px
`;

export const Block = styled.View`
 display: flex;
 flex-direction: row;
 align-items: center
`;

export const TextField = styled.TextInput`
 padding: 5px;
 border-width: 0.556912px;
 border-color: #CFCFCF;
 border-radius: 8px;
 flex: 1
`

export const ButtonView = styled.View`
 width: 35px;
 height: 38px;
 margin-left: 5px
`;

export const ButtonComponent = styled(Button)`
 display: flex;
 align-items: center;
 justify-content: center
`