import styled from 'styled-components/native';
import theme from '../../../assets/theme';
import TextComponent from '../../../components/Text';


export const MainBlock = styled.View`
  padding: 4px;
  max-width: 100px;
  max-height: 100px;
  border-width: 1px;
  margin-right: ${(props) => !props.mg ? 10 : 0}px;
  border-color: ${theme.gray};
  border-radius: 4px
`;

export const TopView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end
`;

export const CloseButton = styled.TouchableOpacity``;

export const ClickableImage = styled.TouchableOpacity`
 max-width: 70px;
 max-height: 90px
`

export const Image = styled.Image`
width: 70px;
height: 60px
`;

export const SizeText =styled(TextComponent)`
 margin-top: 3px
` 