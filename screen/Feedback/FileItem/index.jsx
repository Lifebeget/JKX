import React, { memo, useCallback, useEffect, useMemo, useState } from "react";
import {
    MainBlock,
    TopView,
    CloseButton,
    Image,
    SizeText,
    ClickableImage
} from "./style";
import Close from "../../../assets/svg/close";
import theme from "../../../assets/theme";
import converter from "./converterFile";
import { CONSTANTS } from "../../../actions/constants";
import oth from "../../../assets/img/oth.webp";
import FileViewer from "react-native-file-viewer";
import { useToast } from "react-native-toast-notifications";

const FileItem = (props) => {
    const toast = useToast()
    const [source, setSource] = useState({
        uri: props.uri,
    });
    const [mode, setMode] = useState('cover');

    useEffect(() => {
        if (CONSTANTS.file_extansion_image_type[props.type]) {
            setMode("cover")
            setSource({
                ...{
                    uri: props.uri
                }
            })
            return
        }
        if (CONSTANTS.file_extansion_image[props.type]) {
            setMode("contain")
            setSource(CONSTANTS.file_extansion_image[props.type])
            return
        }
        setSource(oth)
    }, [])

    const fileSize = useMemo(() => {
        return converter(props.size)
    }, [props])

    const handleOpen = useCallback(() => {
        FileViewer.open(props.uri).catch((e) => {
            e.message = 'Нет приложений, связанных с этим типом';
            toast.show(e.message, {
                type: "danger",
                placement: "bottom",
                duration: 4000,
                offset: 30,
                animationType: "slide-in",
            })
        });
    }, [props]);

    return <MainBlock mg={props.index === props.end}>
        <TopView>
            <CloseButton
                onPress={() => {
                    props.onDelete(props.index)
                }}
                activeOpacity={0.5}>
                <Close color={theme.black} />
            </CloseButton>
        </TopView>
        <ClickableImage onPress={handleOpen} activeOpacity={0.5}>
            <Image
                resizeMode={mode}
                source={source} />
            <SizeText color={theme.black} fontSize={9}>{fileSize}</SizeText>
        </ClickableImage>

    </MainBlock>
}

export default memo(FileItem, (prevProps, nextProps) => {
    if (JSON.stringify(prevProps || {}) === JSON.stringify(nextProps || {})) {
        return true
    }
    return false
});
