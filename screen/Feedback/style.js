import styled from 'styled-components/native';
import theme from '../../assets/theme';
import TextComponent from "../../components/Text";
import ButtonComponent from "../../components/Button";

export const MainView = styled.View`
  width: 100%;
  height: 100%;
  flex: 1;
  background-color: ${theme.white};
`

export const HeaderBlock = styled.View`
  width: 100%;
  padding: 5%;
  background-color: ${theme.focused};
  display: flex;
  flex-direction: row;
  align-items: center
`;

export const HeaderTitle = styled(TextComponent)`
  margin-left: 10px
`;

export const FieldContainer = styled.View`
  padding: 15px;
`;

export const UploadView = styled.View`
  display:flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 10px
`;

export const Button = styled(ButtonComponent)`
  border-radius: 5px
`

export const ButtonView = styled.View`
  max-width: ${(props) => props.maxWidth}px
`

export const ButtonText = styled(TextComponent)``;

export const FeedbackBlock = styled.ScrollView`
  width: 100%;
  flex: 1
`;

export const Line = styled.View`
 width: 100%;
 height: 1px;
 background-color: #D3DBE0;
`;

export const FeedbackHeaderBlock = styled.View`
  width: 100%;
  padding: 15px;
  display: flex;
  flex-direction: row
`;

export const LoaderContainer = styled.View`
  flex: 1;
  width: 100%
`

export const Loader = styled.ActivityIndicator``;

export const ImageListBlock = styled.ScrollView`
 display: flex;
 flex-direction: row;
 margin-top: 10px;
 margin-bottom: 10px
`