import React, { useCallback, useEffect, useMemo, useState } from "react";
import HeadNav from "../../components/Header";
import { RefreshControl } from "react-native";
import {
  MainView,
  HeaderBlock,
  HeaderTitle,
  FieldContainer,
  UploadView,
  ButtonText,
  ButtonView,
  Button,
  FeedbackBlock,
  Line,
  FeedbackHeaderBlock,
  Loader,
  LoaderContainer,
  ImageListBlock,
} from "./style";
import Icon from "../../assets/svg/feedback";
import theme from "../../assets/theme";
import TextArea from "../../components/TextArea";
import Upload from "../../components/Upload";
import SendIcon from "../../assets/svg/send";
import { useSelector } from "react-redux";
import FeedbackItem from "./FeedbackItem";
import { useLogout } from "../../actions/hooks/logout";
import {
  fetchRequest,
  uploadFile,
  fetchRequestEntries,
  fetchRequestAdd,
} from "../../actions/fetch";
import { CONSTANTS } from "../../actions/constants";
import FileItem from "./FileItem";

const Feedback = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [file, setFile] = useState([]);
  const [files, setFiles] = useState([]);
  const [value, setValue] = useState("");
  const handleChange = (val) => setValue(val);
  const { width } = useSelector((state) => state.App.Dimensions);
  const logout = useLogout();
  const activeRoom = useSelector((state) => state.Main.activeRoom);
  const ownerInfo = useSelector((state) => state.Login.ownerInfo);
  const rooms = useSelector((state) => state.Main.rooms);
  const [entries, setEntries] = useState({});
  const [load, setLoad] = useState(false);

  const room = useMemo(() => {
    return rooms[activeRoom] ? rooms[activeRoom] : rooms[Object.keys(rooms)[0]];
  }, [rooms]);

  useEffect(() => {
    if (files.length !== 5) {
      setFiles((fileList) => {
        const oldFiles = [...fileList];
        oldFiles.push(...file);
        return oldFiles;
      });
    }
  }, [file]);

  const getRequest = () => {
    setLoading(true);
    return fetchRequest()
      .then((response) => {
        let data = [];
        let obj = {};
        for (let item of response) {
          if (+item.request_id) {
            obj[+item.request_id] = obj[+item.request_id] || [];
            data.push(item);
          }
        }
        setEntries({ ...obj });
        setData([...data]);
        setLoading(false);
      })
      .then(() => {
        return fetchRequestEntries()
          .then((response) => {
            try {
              let obj = { ...entries };
              for (let el of response) {
                if (!obj[+el?.request_id]) {
                  obj[+el.request_id] = [];
                }
                if (
                  !(obj[el.request_id] || []).find(
                    (item) => +item.request_entry_id === +el.request_entry_id
                  )
                ) {
                  obj[+el.request_id].push({ ...el });
                }
              }
              setEntries({ ...obj });
            } catch (e) {
              console.log(e);
              throw new Error(e);
            }
          })
          .catch((e) => {
            if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
              logout();
            }
          });
      })
      .catch((e) => {
        if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
          logout();
        }
      });
  };

  const callRequest = () => {
    let timer = setTimeout(() => {
      fetchRequest()
        .then((response) => {
          let data = [];
          let obj = {};
          for (let item of response) {
            if (+item.request_id) {
              obj[+item.request_id] = obj[+item.request_id] || [];
              data.push(item);
            }
          }
          setData([...data]);
        })
        .then(() => {
          fetchRequestEntries()
            .then((response) => {
              try {
                let obj = { ...entries };
                for (let el of response) {
                  if (!obj[+el?.request_id]) {
                    obj[+el.request_id] = [];
                  }
                  if (
                    !(obj[el.request_id] || []).find(
                      (item) => +item.request_entry_id === +el.request_entry_id
                    )
                  ) {
                    obj[+el.request_id].push({ ...el });
                  }
                }
                setEntries((old) => {
                  old = { ...obj };
                  return old;
                });
                callRequest();
              } catch (e) {
                throw new Error(e);
              }
            })
            .catch((e) => {
              if (
                e?.message?.trim() === CONSTANTS.invalid_token_message.trim()
              ) {
                logout();
              }
              clearTimeout(timer);
            });
        })
        .catch((e) => {
          if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
            logout();
          }
          clearTimeout(timer);
        })
        .then(() => {
          clearTimeout(timer);
        });
    }, 5000);
  };

  useEffect(() => {
    getRequest().then(() => {
      callRequest();
    });
  }, []);

  const onSend = useCallback(() => {
    setLoad(true);
    const body = {
      person_id: ownerInfo?.person_id,
      room_id: room?.room_id,
      source_type: 1,
      subject: CONSTANTS.request_subject,
      text: value,
    };
    return fetchRequestAdd(body).then((request) => {
      try {
        if (request && request.request_id) {
          setData((old) => {
            const d = [...old];
            d.push(request);
            return d;
          });
          const id = request.request_id;
          let promises = [];
          for (let file of files) {
            promises.push(
              new Promise((resolve) => {
                uploadFile(file, id)
                  .then(() => {
                    resolve(null);
                  })
                  .catch((e) => {
                    if (
                      e?.message?.trim() ===
                      CONSTANTS.invalid_token_message.trim()
                    ) {
                      logout();
                    }
                    resolve(null);
                  });
              })
            );
          }
          Promise.all(promises).then(() => {
            setValue('');
            setFile([])
            setLoad(false);
          });
        }
      } catch (e) {}
    });
  }, [files, room, ownerInfo, value]);

  const buttonWidth = useMemo(() => {
    return width / 2.5;
  }, [width]);

  const handleDelete = (index) => {
    return setFiles((filesList) => {
      const newList = [];
      for (let key in filesList) {
        if (key != index) {
          newList.push(filesList[key]);
        }
      }
      return newList;
    });
  };

  return (
    <>
      <HeadNav
        title="Обратная связь"
        menu
        onPress={() => {
          props.navigation.toggleDrawer();
        }}
      />
      <MainView>
        <HeaderBlock>
          <Icon size={37} color={theme.white} />
          <HeaderTitle fontSize={10} fontWeight={"300"}>
            Здесь Вы можете задать вопрос сотрудникам управляющей компании, а
            также оставить заявку на вызов специалиста.
          </HeaderTitle>
        </HeaderBlock>
        <FieldContainer>
          <TextArea
            label="Новое обращение"
            placeholder="Введите текст сообщения"
            value={value}
            handleChange={handleChange}
          />
          {files.length ? (
            <ImageListBlock horizontal={true}>
              {files.map((item, index) => {
                return (
                  <FileItem
                    onDelete={handleDelete}
                    end={files.length - 1}
                    index={index}
                    key={index}
                    {...item}
                  />
                );
              })}
            </ImageListBlock>
          ) : (
            <></>
          )}
          <UploadView>
            <Upload
              disabled={loading || files.length === 5 || load}
              files={file}
              handleChange={setFile}
            />
            <ButtonView maxWidth={buttonWidth}>
              <Button
                disabled={loading || !value?.length || load}
                logo
                icon={SendIcon}
                onPress={onSend}
              >
                <ButtonText fontSize={10}>Отправить</ButtonText>
              </Button>
            </ButtonView>
          </UploadView>
        </FieldContainer>
        {loading ? (
          <LoaderContainer>
            <Loader color={theme.primary} size="large" />
          </LoaderContainer>
        ) : !data.length ? (
          <></>
        ) : (
          <FeedbackBlock
            refreshControl={
              <RefreshControl refreshing={loading} onRefresh={getRequest} />
            }
          >
            <Line />
            <FeedbackHeaderBlock>
              <Icon color={theme.focused} />
              <HeaderTitle fontSize={10} fontWeight={"300"} color={theme.black}>
                Обращения:
              </HeaderTitle>
            </FeedbackHeaderBlock>
            {data.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <FeedbackItem
                    entry={entries[+(item.request_id || 0)]}
                    {...item}
                  />
                </React.Fragment>
              );
            })}
          </FeedbackBlock>
        )}
      </MainView>
    </>
  );
};

export default Feedback;
