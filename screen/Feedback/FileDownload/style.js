import styled from "styled-components/native";
import TextComponent from "../../../components/Text";

export const MainBlock = styled.TouchableOpacity`
  padding: 5px;
  border-width: 1px;
  border-color: rgba(60, 68, 110, 0.3);
  display: flex;
  height: 35px;
  max-width: 200px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-radius: 86px;
  margin-top: 5px;
  margin-left: ${(props) => (props.index % 2 == 0 ? 0 : 5)}px;
`;

export const FileNameText = styled(TextComponent)`
  margin-left: 5px
`;
