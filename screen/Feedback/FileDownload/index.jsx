import React, { memo, useCallback, useMemo } from "react";
import { MainBlock, FileNameText } from "./style";
import Copy from "../../../assets/svg/copy";
import theme from "../../../assets/theme";
import { fetchToken } from "../../../actions/fetch";
import { Platform, PermissionsAndroid } from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import { useToast } from "react-native-toast-notifications";

const FileDownload = ({ file, index }) => {
  const toast = useToast();
  const fileName = useMemo(() => {
    if (!file?.original_file_name) {
      return "";
    }
    try {
      const fileName = (file?.original_file_name || "")?.split(".")[0];
      const ext = (file?.original_file_name || "")?.split(".")[1];
      return `${fileName.match(/.{1,11}/g)[0]}.${ext}`;
    } catch (e) {
      return "";
    }
  }, [file]);

  const checkPermission = async (token, url) => {
    if (Platform.OS === "ios") {
      downloadFile(token, url);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Требуется разрешение на хранение",
            message:
              "Приложению требуется доступ к вашему хранилищу для загрузки файла",
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(token, url);
        } else {
          toast.show("Разрешение на хранение не предоставлено", {
            type: "danger",
            placement: "bottom",
            duration: 4000,
            offset: 30,
            animationType: "slide-in",
          });
        }
      } catch (err) {
        console.log("++++" + err);
      }
    }
  };

  const downloadFile = (token, url) => {
    const fileUrl = `${url}api/uploads/${file.upload_id}/`;
    let FILE_URL = fileUrl;
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path: `${RootDir}/${file.original_file_name}`,
        description: "скачивание файла...",
        notification: true,
        useDownloadManager: true,
      },
    };
    config(options)
      .fetch("GET", FILE_URL, {
        Authorization: `Bearer ${token}`,
      })
      .then((res) => {
        toast.show("Файл успешно загружен.", {
          type: "success",
          placement: "bottom",
          duration: 4000,
          offset: 30,
          animationType: "slide-in",
        });
      });
  };

  const onDownload = useCallback(() => {
    return fetchToken().then(({ token, baseUrl }) => {
      if (token) {
        checkPermission(token, baseUrl);
      }
    });
  }, [file]);

  return (
    <MainBlock index={index} onPress={onDownload}>
      <Copy color={theme.primary} />
      <FileNameText color={"#8893A6"}>{fileName}</FileNameText>
    </MainBlock>
  );
};

export default memo(FileDownload, (prevProps, nextProps) => {
  if (JSON.stringify(prevProps) === JSON.stringify(nextProps)) {
    return true;
  }
  return false;
});
