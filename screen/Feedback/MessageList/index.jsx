import React, { memo, useEffect, useState } from "react";
import MessageItem from "../MessageItem";
import { MainBlock } from "./style";
const MessageList = ({ list }) => {
  const [entry, setEntry] = useState([]);
  useEffect(() =>{
    let lists = [...list];
    for (let [index,item] of lists.entries()){
      if (lists.filter(el => +(el?.request_entry_id || 0) === +(item?.request_entry_id || 0)).length > 1){
        lists.slice(index, 1)
      }
    }
    setEntry([...(lists.reverse())])
  },[list])
  return (
    <MainBlock>
      {entry.map((item, index) => {
        return <MessageItem key={index} item={item} />;
      })}
    </MainBlock>
  );
};

export default memo(MessageList, (prevProps, nextProps) => {
  if (prevProps.list.length === nextProps.list.length) {
    return true;
  }
  return false;
});
