import styled from 'styled-components/native';

export const MainBlock = styled.View`
 padding-left: 15px;
 padding-right: 15px;
 padding-top: 5px;
 flex: 1
`