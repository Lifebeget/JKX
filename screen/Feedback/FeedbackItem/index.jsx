import React, { memo, useCallback, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import {
  MainBlock,
  TopBlock,
  TitleText,
  ArrowBlock,
  BottomBlock,
  StatusBlock,
  StatusText,
  DateLabel,
  DateText,
  MessageMainBlock,
} from "./style";
import Up from "../../../assets/svg/up";
import theme from "../../../assets/theme";
import { CONSTANTS } from "../../../actions/constants";
import FieldBlock from "../FieldBlock";
import {
  fetchFilesRequest,
  fetchRequestEntries,
  fetchRequestEntryAdd,
  uploadFile,
} from "../../../actions/fetch";
import MessageList from "../MessageList";
import { useLogout } from "../../../actions/hooks/logout";

const FeedbackItem = (props) => {
  const { width } = useSelector((state) => state.App.Dimensions);
  const [open, setOpen] = useState(false);
  const [entryList, setEntryList] = useState([]);
  const logout = useLogout();

  const entry = useMemo(() => {
    let entr = [...(props?.entry || [])];
    entr = entr.sort((a, b) => {
      return (
        new Date((b?.created || 0) * 1000) - new Date((a?.created || 0) * 1000)
      );
    });
    return entr;
  }, [props.entry]);

  useEffect(() => {
    fetchFilesRequest(props.request_id).then((files) => {
      const list = entry.length ? entry : [{ status: 1 }];
      const entries = [];
      entries.push({
        author_employee_id: null,
        author_owner_id: null,
        created: props.created,
        employee_id: null,
        request_entry_id: null,
        request_id: props.request_id,
        status: list[0]?.status || 1,
        text: props.text,
        files,
      });
      const entryLists = entry.sort((a, b) => {
        return (
          new Date((a?.created || 0) * 1000) -
          new Date((b?.created || 0) * 1000)
        );
      });
      for (let e of entryLists) {
        entries.push({
          ...e,
          files: [],
        });
      }
      setEntryList([...entries]);
    });
    return () => {};
  }, [entry]);

  const status = useMemo(() => {
    const list = entry.length ? entry : [{ status: 1 }];
    return (
      CONSTANTS.request_status[list[0]?.status || 1] ||
      CONSTANTS.request_status[0]
    );
  }, [entry]);

  const statusColor = useMemo(() => {
    const list = entry.length ? entry : [{ status: 1 }];
    return (
      CONSTANTS.request_status_color[list[0]?.status || 1] ||
      CONSTANTS.request_status_color[0]
    );
  }, [entry]);

  const date = useMemo(() => {
    let d = new Date(props.created * 1000);
    return `${
      String(d.getDate()).length === 1 ? "0" + d.getDate() : d.getDate()
    }.${
      String(d.getMonth()).length === 1 ? "0" + d.getMonth() : d.getMonth()
    }.${d.getFullYear()}`;
  }, [props]);

  const handleOpen = useCallback(() => {
    setOpen(!open);
  }, [open]);

  const handleSend = (value, files) => {
    const body = { request_id: props?.request_id, text: value };
    return new Promise((resolve, reject) => {
      const lists = [...entryList];
      fetchRequestEntryAdd(body)
        .then((response) => {
          if (response) {
            if (response.request_entry_id) {
              lists.push({ ...response });
            }
          }
          resolve(lists);
        })
        .catch((e) => {
          if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
            logout();
            return;
          }
          if (e?.message?.trim() === CONSTANTS.request_entry_message.trim()) {
            return fetchRequestEntries(props?.request_id).then((response) => {
              resolve([...(response || [])]);
            });
          }
          reject(e);
        });
    })
      .then((data) => {
        let id = null;
        const lists = [...entryList];
        for (let item of data) {
          try {
            if (
              !lists.find(
                (i) =>
                  +(i?.request_entry_id || 0) === +(item?.request_entry_id || 0)
              )
            ) {
              lists.push({ ...item });
            }
          } catch (e) {}
        }
        const sortList = lists.sort((a, b) => {
          return (
            new Date((a?.created || 0) * 1000) -
            new Date((b?.created || 0) * 1000)
          );
        });
        if (lists.length > entryList.length) {
          try {
            if (sortList[(sortList?.length || 1) - 1]) {
              if (
                sortList[(sortList?.length || 1) - 1]?.request_entry_id !==
                entryList[(entryList?.length || 0) - 1]?.request_entry_id
              ) {
                id = sortList[(sortList?.length || 1) - 1]?.request_entry_id;
              }
            }
          } catch (e) {}
        }
        let l = [...lists];
        for (let [index, item] of l.entries()) {
          if (
            l.filter(
              (el) =>
                +(el?.request_entry_id || 0) === +(item?.request_entry_id || 0)
            ).length > 1
          ) {
            l.slice(index, 1);
          }
        }
        setEntryList([...l]);
        return id;
      })
      .then((id) => {
        if (id) {
          let promises = [];
          for (let file of files) {
            promises.push(
              new Promise((resolve) => {
                uploadFile(file, id, "request_entry")
                  .then(() => {
                    resolve(null);
                  })
                  .catch((e) => {
                    if (
                      e?.message?.trim() ===
                      CONSTANTS.invalid_token_message.trim()
                    ) {
                      logout();
                    }
                    resolve(null);
                  });
              })
            );
          }
          Promise.all(promises);
        }
      });
  };

  return (
    <>
      <MainBlock activeOpacity={0.9} open={open} onPress={handleOpen}>
        <TopBlock>
          <TitleText maxWidth={width} fontSize={13} fontWeight="400">
            {props?.subject || ""}
          </TitleText>
          <ArrowBlock open={open}>
            <Up color={theme.white} />
          </ArrowBlock>
        </TopBlock>
        <BottomBlock maxWidth={width}>
          <StatusBlock color={statusColor}>
            <StatusText fontSize={9}>{status}</StatusText>
          </StatusBlock>
          <DateLabel fontWeight={"300"} fontSize={10}>
            Задача создана:
          </DateLabel>
          <DateText fontWeight={"600"} fontSize={11}>
            {date}
          </DateText>
        </BottomBlock>
      </MainBlock>
      {open ? (
        <React.Fragment>
          <MessageList list={entryList} />
          <MessageMainBlock>
            <FieldBlock handleSend={handleSend} />
          </MessageMainBlock>
        </React.Fragment>
      ) : (
        <></>
      )}
    </>
  );
};

export default memo(FeedbackItem);
