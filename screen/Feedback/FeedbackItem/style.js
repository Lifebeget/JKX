import styled from 'styled-components/native';
import theme from '../../../assets/theme';
import TextComponent from '../../../components/Text';

export const MainBlock = styled.TouchableOpacity`
  width: 100%;
  background-color: ${(props) => props.open ? 'rgba(3, 91, 162, 0.8)' : theme.primary};
  padding: 15px;
  border-bottom-width: 1px;
  border-bottom-color: ${theme.white};

`;

export const TopBlock = styled.View`
 display: flex;
 flex-direction: row;
 justify-content: space-between
`;

export const TitleText = styled(TextComponent)`
 max-width: ${(props) => props.maxWidth - 70}px
`;

export const ArrowBlock = styled.View`
 display: flex;
 align-items: center;
 justify-content: center;
 transform: rotate(${(props) => props.open ? '180' : '0'}deg)
`;


export const BottomBlock = styled.View`
 max-width: ${(props) => props.maxWidth - 70}px;
 display: flex;
 flex-direction: row;
 align-items: center;
 margin-top: 5px
`

export const StatusBlock = styled.View`
  padding: 5px;
  background-color: ${(props) => props.color}
`;

export const StatusText = styled(TextComponent)``;

export const DateLabel = styled(TextComponent)`
  margin-left: 5px
`;

export const DateText = styled(TextComponent)`
 margin-left: 5px
`;

export const MessageMainBlock = styled.View`
  width: 100%;
  padding: 15px;
`;