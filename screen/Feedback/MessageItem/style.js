import styled from 'styled-components/native';
import TextComponent from '../../../components/Text';

export const MainBlock = styled.View`
 display: flex;
 flex-direction: column;
 border-bottom-width: 1px;
 padding: 5px;
 flex: 1;
 border-bottom-color: #E8EDF0;
 background-color: ${(props) => !props.check ? '#e1f5fe' : '#ffffff'}
`;

export const TopBlock = styled.View`
 display:flex;
 flex-direction:row;
 justify-content: space-between
`;

export const Text = styled(TextComponent)``;

export const TopLeftBlock = styled.View`
 max-width: ${(props) => props.maxWidth - 100}px;
 display: flex;
 flex-direction: row;
 align-items: center
`;

export const TopRightBlock = styled.View`
 max-width: 100px;
 display: flex;
 flex-direction: row;
 justify-content: flex-end;
 align-items: center
`;

export const BottomBlock = styled.View`
 max-width: ${(props) => props.maxWidth - 100}px;
 display: flex;
 flex: 1;
 flex-direction: column;
 margin-top: 3px
`;

export const FileBlock = styled.View`
 flex: 1;
 width: 100%;
 display: flex;
 margin-top: 3px;
 flex-wrap: wrap;
 flex-direction: row;
`