import React, { memo, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { CONSTANTS } from "../../../actions/constants";
import { fetchFilesRequestEntry } from "../../../actions/fetch";
import FileDownload from "../FileDownload";
import {
  MainBlock,
  TopBlock,
  Text,
  TopLeftBlock,
  TopRightBlock,
  BottomBlock,
  FileBlock,
} from "./style";

const MessageItem = ({ item }) => {
  const { width } = useSelector((state) => state.App.Dimensions);
  const ownerInfo = useSelector((state) => state.Login.ownerInfo);
  const [files, setFiles] = useState([]);

  useEffect(() => {
    if (!item.request_entry_id) {
      setFiles([...(item?.files || [])]);
    } else {
      return fetchFilesRequestEntry(item.request_entry_id).then((response) => {
        setFiles([...((response || []) instanceof Array ? response : [])]);
      });
    }
    return () => {};
  }, [item]);

  const getPerson = useMemo(() => {
    let user = item.employee || item.owner;
    if (!item.request_entry_id) {
      user = { person: ownerInfo };
    }
    return (
      user?.person || {
        email: null,
        first_name: "",
        gender: null,
        last_name: "",
      }
    );
  }, [item]);

  const checkMe = useMemo(() => {
    return +(getPerson?.person_id || 0) === +(ownerInfo?.person_id || 0);
  }, [getPerson]);

  const personFio = useMemo(() => {
    const fio = `${getPerson?.first_name || ""} ${getPerson?.last_name || ""}`;
    return checkMe ? "Вы" : fio;
  }, [checkMe]);

  const personStatus = useMemo(() => {
    const owner = item.employee || item.owner;
    return owner?.appointment ? `(${owner?.appointment})` : "";
  }, [item]);

  const time = useMemo(() => {
    const date = new Date(
      (item?.created || new Date().getTime() / 1000) * 1000
    );
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    let d = "";
    if (year === new Date().getFullYear()) {
      if (month === new Date().getMonth() && day === new Date().getDate()) {
        d = date.toLocaleTimeString().split("").slice(0, 5).join("");
      } else {
        d = `${String(day).length === 1 ? `0${day}` : day} ${
          CONSTANTS.month[month - 1]
        }.`;
      }
    } else {
      d = `${year} ${CONSTANTS.month[month - 1]}.`;
    }
    return d;
  }, [item]);

  return (
    <MainBlock check={checkMe}>
      <TopBlock>
        <TopLeftBlock maxWidth={width}>
          <Text color={"#1F2021"} fontSize={13}>
            {personFio}{" "}
            <Text color={"#8893A6"} fontSize={12}>
              {personStatus}
            </Text>
          </Text>
        </TopLeftBlock>
        <TopRightBlock>
          <Text color={"#8893A6"} fontSize={11}>
            {" "}
            {time}
          </Text>
        </TopRightBlock>
      </TopBlock>
      <BottomBlock maxWidth={width}>
        <Text color={"#1F2021"} fontWeight={"300"} fontSize={12}>
          {item?.text || ""}
        </Text>
        <FileBlock>
          {files.map((item, index) => {
            return <FileDownload file={item} index={index} key={index} />;
          })}
        </FileBlock>
      </BottomBlock>
    </MainBlock>
  );
};

export default memo(MessageItem, (prevProps, nextProps) => {
  if (JSON.stringify(prevProps) === JSON.stringify(nextProps)) {
    return true;
  }
  return false;
});
