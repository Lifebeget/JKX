import React, { memo, useCallback, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import theme from "../../../../assets/theme";
import { MainBlock, LeftBlock, Text, BlockLeft, StatusBlock, OpenBlock, OpenBlockChild } from "./style";
import InfoIcon from "../../../../assets/svg/info";
import moment from "moment";
import QuestionItem from "../QuestionItem";

const PollsItem = ({ data, documentTypes }) => {
  const { width } = useSelector((state) => state.App.Dimensions);
  const show_status = data?.obj?.type === 'finish' || data?.obj?.type === 'future';
  const [open, setOpen] = useState(false);
  const handleOpen = useCallback(() =>{
     setOpen(!open)
  },[open])
  const [questions, setQuestions] = useState((data?.obj?.question || []));
  
  const personName = useMemo(() =>{
     return `${data?.obj?.person?.last_name ? `${data?.obj?.person?.last_name}  ` : ''}${data?.obj?.person?.first_name || ''}`
  },[data]);

  const setVote = (question_id, vote) =>{
    let quest = [...questions];
    for (let key in quest){
      if (+quest[key]?.question_id === +question_id){
         quest[key].vote = vote
      }
    }
    setQuestions([...quest])
  };

  return (
    <>
    <MainBlock activeOpacity={0.7} onPress={handleOpen}>
      <LeftBlock maxWidth={width}>
        <BlockLeft fullWidth={!show_status}>
         <Text color={theme.black} fontSize={9.8}>
          {data?.obj?.text}
         </Text>
         <Text color={theme.black} fontSize={9}>
          с {moment(data?.obj?.start_date || new Date()).format('DD.MM.YYYY')} {moment(data?.obj?.start_date || new Date()).format('HH:MM')} до {moment(data?.obj?.end_date || new Date()).format('DD.MM.YYYY')}
         </Text>
        </BlockLeft>
         {
            data?.obj?.type === 'finish' ? <StatusBlock bg="#50AE55">
            <Text fontSize={9}>
              Голосование состоялось
            </Text>
          </StatusBlock> : <>
          {
            data?.obj?.type === 'future' ? <StatusBlock bg="#8893A6">
               <Text fontSize={9}>
                 Голосование не состоялось
               </Text>
            </StatusBlock> : <></>
          }
          </>
         }
      </LeftBlock>
       <InfoIcon color="#035BA2" size={16}/>
    </MainBlock>
    {
      open ? <>
      <OpenBlock>
        <OpenBlockChild maxWidth={width}>
           <Text fontSize={9.5} color="#8893A6">
             Место проведения
           </Text>
           <Text
           style={{
            marginTop: 5,
            marginBottom: 5
           }}
           color="#1F2021" fontSize={9}>
             {data?.obj?.address || data?.address}
           </Text>
           <Text fontSize={9.5} color="#8893A6">
             Ответственный
           </Text>
           <Text
           style={{
            marginTop: 5,
            marginBottom: 5
           }}
           color="#1F2021" fontSize={9}>
             {personName}
           </Text>
        </OpenBlockChild>
      </OpenBlock>
      <>
      {
        questions.map((item, index) =>{
          return <QuestionItem 
          index={index}
          data={item}
          key={index}
          setVotes={setVote}
          documentTypes={documentTypes}
          type={data?.obj?.type}
          typeString={data.type || (data?.obj?.poll_type === 1 ? 'active' : data.type)}
          />
        })
      }
      </>
      </> :
       <></>
    }
    </>
  );
};

export default memo(PollsItem, (prevProps, nextProps) =>{
  if (JSON.stringify(prevProps) === JSON.stringify(nextProps)){
    return true
  }
  return false
});
