import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.TouchableOpacity`
 flex: 1;
 display: flex;
 flex-direction: row;
 align-items: center;
 padding: 8px 16px;
 border-bottom-color: rgba(70, 65, 65, 0.2);
 border-bottom-width: 1px;
 justify-content: space-between
`;

export const LeftBlock = styled.View`
  max-width: ${(props) => +(props?.maxWidth || 320) - 50}px;
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center
`

export const Text = styled(TextComponent)`
 line-height: 13px
`;

export const BlockLeft= styled.View`
  flex: 1;
  ${(props) => props.fullWidth ? 'width: 100%': ''};
`;

export const StatusBlock = styled.View`
  max-width: 100px;
  background-color: ${(props) => props.bg};
  padding: 5px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-left: 10px
`;

export const OpenBlock = styled.View`
 flex: 1;
 background-color: #F3F6FB;
 padding-left: 20px;
 padding-right: 20px;
 padding-top: 8px;
 padding-bottom: 8px
`;

export const OpenBlockChild = styled.View`
 border-left-color: #86A7D1;
 border-left-width: 1px;
 flex: 1;
 max-width: ${(props) => +(props?.maxWidth || 320) - 50}px;
 padding: 2px
`