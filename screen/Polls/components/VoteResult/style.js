import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.View`
 flex: 1;
 border-top-width: 1px;
 border-top-color: #D3DBE0;
 margin-left: 20px;
 margin-right: 20px;
 padding-top: 5px;
 padding-bottom: 5px
`;

export const FlexBlock = styled.View`
 flex: 1;
 display: flex;
 flex-direction: row;
 align-items: center;
 margin-top: 5px
`;

export const Text = styled(TextComponent)`
 text-transform: lowercase;
 margin-left: 10px
`