import React from "react";
import { memo } from "react";
import { MainBlock, FlexBlock, Text } from "./style";
import LikeIcon from "../../../../assets/svg/like";
import DisLikeIcon from "../../../../assets/svg/dislike";
import LikeAndDislike from "../../../../assets/svg/likeanddislike";

const VoteResult = ({question}) =>{
    return <MainBlock>
       <FlexBlock>
          <LikeIcon color="#50AE55"/>
          <Text color="#50AE55" fontSize={10}>{question?.result?.likes || 0}% поддержали</Text>
       </FlexBlock>
       <FlexBlock>
          <DisLikeIcon color="#A21B1B"/>
          <Text color="#A21B1B" fontSize={10}>{question?.result?.dislikes || 0}% не поддержали</Text>
       </FlexBlock>
       <FlexBlock>
          <LikeAndDislike/>
          <Text color="#586781" fontSize={10}>{question?.result?.ignore || 0}% воздержались</Text>
       </FlexBlock>
    </MainBlock>
}

export default memo(VoteResult)