import React, { memo, useEffect, useState } from "react";
import { MainBlock, IconButton, Text } from "./style";
import LikeIcon from "../../../../assets/svg/like";
import DisLikeIcon from "../../../../assets/svg/dislike";
import LikeAndDislike from "../../../../assets/svg/likeanddislike";
import { useSelector } from "react-redux";
import { fetchSendPostVote, fetchSendPutVote } from "../../../../actions/fetch";
import { CONSTANTS } from "../../../../actions/constants";
import { useLogout } from "../../../../actions/hooks/logout";
import { useAsyncStorage } from "@react-native-async-storage/async-storage";

const VoteButtons = ({ question, setVotes }) => {
  const [width, setWidth] = useState(320);
  const [vote, setVote] = useState([]);
  const [loading, setLoading] = useState(false);
  const ownerInfo = useSelector((state) => state.Login.ownerInfo);
  const logout = useLogout();
  const storage = useAsyncStorage('ownerInfo')
  useEffect(() => {
    setVote(question.vote);
  }, [question]);

  const changeVote = (votes) => {
    setLoading(true);
    let promise = Promise.resolve(null);
    let getOwner = Promise.resolve({});
     if (!ownerInfo?.owner_id){
      getOwner = storage.getItem().then(data =>{
        try{
          return JSON.parse(data)
        }catch(e){
          return {}
        }
      })
     }
    if (typeof vote === "number") {
      promise = getOwner.then(owner =>{
        return fetchSendPutVote(
          { vote: votes },
          question?.question_id,
          ownerInfo?.owner_id || owner?.owner_id
        )
      }).catch(e =>{
        if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
          logout();
        }else{
         setLoading(true);
         return fetchSendPostVote({
            vote: votes,
            owner_id: ownerInfo?.owner_id,
            question_id: question?.question_id || owner?.owner_id,
          }).then((response) => {
            setLoading(false);
            if (response) {
              setVote(response?.vote || votes);
              setVotes(response?.vote || votes)
            }
          })
        }
      });
    } else {
      promise = getOwner.then((owner) =>{
        return fetchSendPostVote({
          vote: votes,
          owner_id: ownerInfo?.owner_id,
          question_id: question?.question_id || owner?.owner_id,
        })
      });
    }
    promise
      .then((response) => {
        setLoading(false);
        if (response) {
          setVote(response?.vote || votes);
          setVotes(response?.vote || votes)
        }
      })
      .catch((e) => {
        console.warn(e)
        setLoading(false);
        if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
          logout();
        }
      });
  };

  return (
    <MainBlock
      onLayout={(e) => {
        setWidth(+(e?.nativeEvent?.layout?.width || 320));
      }}
    >
      <IconButton
        onPress={() => changeVote(1)}
        activeOpacity={0.8}
        loading={loading}
        minWidth={width / 3}
        disabled={loading}
      >
        <LikeIcon
          width={18.33}
          height={16.33}
          color={vote === 1 && !loading ? "#50AE55" : "#586781"}
        />
        <Text
          fontSize={10}
          color={vote === 1 && !loading ? "#50AE55" : "#586781"}
        >
          за
        </Text>
      </IconButton>
      <IconButton
        onPress={() => changeVote(-1)}
        activeOpacity={0.8}
        minWidth={width / 3}
        loading={loading}
        disabled={loading}
      >
        <DisLikeIcon
          width={18.33}
          height={16.33}
          color={vote === -1 && !loading ? "#A21B1B" : "#586781"}
        />
        <Text
          fontSize={10}
          color={vote === -1 && !loading ? "#A21B1B" : "#586781"}
        >
          Против
        </Text>
      </IconButton>
      <IconButton
        onPress={() => changeVote(0)}
        activeOpacity={0.8}
        minWidth={width / 3}
        disabled={loading}
        loading={loading}
      >
        <LikeAndDislike width={18.33} height={16.33} color={"#586781"} />
        <Text fontSize={10} color={"#586781"}>
          воздержался
        </Text>
      </IconButton>
    </MainBlock>
  );
};

export default memo(VoteButtons);
