import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.View`
 padding-left: 20px;
 padding-right: 20px;
 padding-top: 8px;
 padding-bottom: 8px;
 flex: 1;
 width: 100%;
 display: flex;
 flex-direction: row;
 justify-content: space-between
`;

export const IconButton = styled.TouchableOpacity`
  min-width: ${(props) => props.minWidth - 30}px;
  background-color: #F3F6FB;
  padding: 7px;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: ${props => props.loading ? '0.5' : '1'}
`;

export const Text = styled(TextComponent)`
 text-transform: uppercase;
 margin-top: 3px;
`