import React, { memo, useMemo, useState } from "react";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { useSelector } from "react-redux";
import { Text, MainBlock } from "./style";
import Colors from "../../../../assets/theme";
import PollsTabItem from "../PollsTabItem";

const renderScene = SceneMap({
  0: (props) => {
    return <PollsTabItem {...props} />;
  },
  1: (props) => {
    return <PollsTabItem {...props} />;
  },
  2: (props) => {
    return <PollsTabItem {...props} />;
  },
});

const LazyPlaceholder = () => (
  <MainBlock>
    <Text color={Colors.black}>Загрузка...</Text>
  </MainBlock>
);

const PoolsTab = (props) => {
  const [index, setIndex] = useState(0);
  const Dimensions = useSelector((state) => state.App.Dimensions);
  const initialLayout = { width: Dimensions.width };

  const routes = useMemo(() => {
    let types_polls = {
      0: [],
      1: [],
      2: []
    };
    for (let poll of (props.polls || [])){
     try{
      types_polls[poll.obj.poll_type].push(poll)
     }catch(e){}
    }
    let documentTypes = {}
    for (let item of (props?.documentTypes || [])){
      documentTypes[item?.document_type_id] = item
    }
    let data = [
      {
        key: 0,
        title: "Результаты",
        props: {
          onRefresh: props.onRefresh,
          loading: props.loading,
          polls: types_polls[0],
          documentTypes: documentTypes
        }
      },
      {
        key: 1,
        title: "Актуальные",
        props: {
          onRefresh: props.onRefresh,
          loading: props.loading,
          polls: types_polls[1],
          documentTypes: documentTypes
        }
      },
      {
        key: 2,
        title: "Запланированные",
        props: {
          onRefresh: props.onRefresh,
          loading: props.loading,
          polls: types_polls[2],
          documentTypes: documentTypes
        }
      },
    ];
    return data;
  }, [renderScene, props]);

  return (
    <MainBlock>
      <TabView
        lazy
        renderLazyPlaceholder={() => <LazyPlaceholder />}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        renderTabBar={(props) => {
          return (
            <TabBar
              inactiveColor="rgba(70, 65, 65, 0.6)"
              activeColor={Colors.focused}
              pressColor="rgba(3, 91, 162, 0.1)"
              indicatorStyle={{
                backgroundColor: Colors.focused,
                elevation: 1,
                height: 0,
              }}
              renderLabel={({ route, color }) => {
                return (
                  <Text
                    fontSize={9.5}
                    style={{
                      textAlign: "center",
                      borderBottomColor: color,
                      borderBottomWidth: color !== Colors.focused ? 0 : 1,
                      padding: 5,
                      textTransform: 'uppercase'
                    }}
                    color={color}
                  >
                    {route.title}
                  </Text>
                );
              }}
              {...props}
              style={{
                maxHeight: 150,
                backgroundColor: "#D9E7F2",
                shadowColor: "transparent",
                shadowOffset: { height: 5 },
                shadowOpacity: 0.4,
                borderBottomWidth: 1,
                borderBottomColor: "rgba(217, 231, 242, 0.4)",
                padding: 0,
              }}
            />
          );
        }}
      />
    </MainBlock>
  );
};

export default memo(PoolsTab,(prevProps, nextProps)=>{
  if (JSON.stringify(prevProps) === JSON.stringify(nextProps)){
    return true
  }
  return false
});
