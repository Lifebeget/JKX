import styled from 'styled-components/native';


export const MainBlock = styled.ScrollView`
 flex: 1;
`;

export const LoadingBlock = styled.View`
 flex: 1;
 width: 100%;
 display: flex;
 flex-direction: row;
 align-items: center;
 justify-content: center;
 padding: 20px
`;

export const Loader = styled.ActivityIndicator``