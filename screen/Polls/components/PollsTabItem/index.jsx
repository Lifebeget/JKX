import React, { memo, useMemo } from "react";
import { MainBlock, LoadingBlock, Loader } from "./style";
import { RefreshControl } from "react-native";
import NoExistPolls from "../NoExistPolls";
import theme from "../../../../assets/theme";
import PollsItem from "../PollsItem";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const PollsTabItem = (props) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const loading = useMemo(() => {
    return props?.route?.props?.loading;
  }, [props]);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(100).then(() => {
      props?.route?.props?.onRefresh();
      setRefreshing(false);
    });
  }, []);
  
  const polls = useMemo(() =>{
    return props?.route?.props?.polls || []
  },[props])
  
  const documentTypes = useMemo(() =>{
    return props?.route?.props?.documentTypes || []
  },[props]);

  return (
    <MainBlock
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      {loading ? (
        <LoadingBlock>
          <Loader size="large" color={theme.focused} />
        </LoadingBlock>
      ) : 
        !polls.length ? <NoExistPolls /> : <>
          {
            polls.map((item, index) =>{
              return <PollsItem 
              data={item}
              key={index}
              documentTypes={documentTypes}
              />
            })
          }
        </>
      }
    </MainBlock>
  );
};

export default memo(PollsTabItem, (prevProps, nextProps) => {
  if (JSON.stringify(prevProps) === JSON.stringify(nextProps)) {
    return true;
  }
  return false;
});
