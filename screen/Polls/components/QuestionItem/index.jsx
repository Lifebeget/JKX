import React, { memo, useCallback, useState } from "react";
import {
  MainBlock,
  QuestionMainBlock,
  QuestionTitleBlock,
  Text,
  RightBlock,
  StatusBlock,
  OpenedBlcok,
  OpenedHeaderBlock,
  OpenedHeaderChildBlock
} from "./style";
import InfoIcon from "../../../../assets/svg/info";
import VoteButtons from "../VoteButtons";
import VoteResults from "../VoteResult";
import FileBlock from "../FileBlock";

const importanceType = {
  0: 'Не важный',
  1: 'Важный',
  2: 'Очень важный'
};
const quorums = {
  1: '1/2',
  2: '2/3'
};
const QuestionItem = ({ data, index, typeString, type, documentTypes, setVotes }) => {
  const [open, setOpen] = useState(false);
  
  const setVote = (vote) =>{
    setVotes(data.question_id, vote)
  }

  const handleOpen = useCallback(() =>{
     setOpen(!open)
  },[open]);
  
  return (
    <MainBlock>
      <QuestionMainBlock activeOpacity={0.9} onPress={handleOpen}>
        <QuestionTitleBlock>
          <Text fontSize={10}>Вопрос №{index + 1}</Text>
        </QuestionTitleBlock>
        <RightBlock>
          {typeof data.vote !== 'number' && typeString === "active" ? (
            <StatusBlock bgColor="#fbac29">
              <Text fontSize={9.5}>Вы не проголосовали</Text>
            </StatusBlock>
          ) : (
            <>
              {data?.result && data?.result?.status ? (
                <>
                  <StatusBlock bgColor="#a21b1b">
                    <Text fontSize={9.5}>{data?.result?.status}</Text>
                  </StatusBlock>
                </>
              ) : (
                <></>
              )}
            </>
          )}
          <InfoIcon color="#ffffff" size={16} />
        </RightBlock>
      </QuestionMainBlock>
      {
        open ? <OpenedBlcok>
           {importanceType[data?.type] || quorums[data?.type] ? <OpenedHeaderBlock>
              <OpenedHeaderChildBlock>
                {
               importanceType[data?.type] ? <Text color="#464141" fontSize={9.5}>
                  Тип вопроса: <Text fontWeight="700" color="#464141" fontSize={9.5}>
                    {importanceType[data?.type]}
                  </Text>
                </Text> : <></>
                }
               {quorums[data?.type] ? <Text color="#464141" fontSize={9.5}>
                  Минимальный кворум по этому вопросу: 
                  <Text fontWeight="700" color="#464141" fontSize={9.5}>
                    {quorums[data?.type]}
                  </Text>
                </Text> : <></>}
              </OpenedHeaderChildBlock>
            </OpenedHeaderBlock> : <></>
          }
          <>
          {data?.title ? <Text 
          fontSize={11}
          fontWeight="700"
          style={{
            marginLeft: 20,
            marginRight: 20,
            marginTop: 8
          }}
          color="#464141">
             {data?.title}
          </Text> :<></>}
          {data?.text ? <Text 
          fontSize={11}
          style={{
            marginLeft: 20,
            marginRight: 20,
            marginTop: data?.title ? 2 : 8,
            marginBottom: 8
          }}
          color="#464141">
             {data?.text}
          </Text> :<></>}
          </>
          {
            type === 3 && typeString === 'active' ? <VoteButtons 
            question={data}
            setVotes={setVote}
            /> : <></>
          }
          {
            typeString === 'finish' && data?.result ? <VoteResults question={data}/> : <></>
          }
          <FileBlock documentTypes={documentTypes} question_id={data?.question_id}/>
        </OpenedBlcok> : <></>
      }
    </MainBlock>
  );
};

export default memo(QuestionItem);
