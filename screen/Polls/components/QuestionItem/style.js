import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.View`
 width: 100%;
 flex: 1;
 background-color: #ffffff
`;

export const QuestionMainBlock = styled.TouchableOpacity`
 width: 100%;
 flex: 1;
 padding-left: 20px;
 padding-right: 20px;
 padding-top: 8px;
 padding-bottom: 8px;
 background-color: #035BA2;
 display: flex;
 flex-direction: row;
 justify-content: space-between;
 align-items: center;
 border-bottom-width: 1px;
 border-bottom-color: #FCFEFF
`;

export const QuestionTitleBlock = styled.View`
 padding: 5px;
 background-color: #464141
`;

export const Text = styled(TextComponent)``;

export const RightBlock = styled.View`
 display: flex;
 flex-direction: row;
 align-items: center
`;

export const StatusBlock = styled.View`
 padding: 2px;
 background-color: ${(props) => props.bgColor};
 margin-right: 5px
`;

export const OpenedBlcok = styled.View`
 width: 100%;
 flex: 1
`;

export const OpenedHeaderBlock = styled.View`
padding-left: 20px;
padding-right: 20px;
padding-top: 8px;
padding-bottom: 8px;
background-color: #F3F6FB;
width: 100%;
flex: 1
`;

export const OpenedHeaderChildBlock = styled.View`
 flex: 1;
 width: 100%;
 border-left-color: #86A7D1;
 border-left-width: 1px;
 padding: 2px
`;

export const OpenTitleBlock = styled.View`
margin-left: 20px;
margin-right: 20px;
margin-top: 8px;
margin-bottom: 8px;
`;