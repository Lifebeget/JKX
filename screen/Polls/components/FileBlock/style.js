import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.View`
 flex: 1;
 width: 100%;
 padding-left: 20px;
 padding-right: 20px
`;


export const BlockItem = styled.View`
  flex: 1;
  width: 100%;
  margin-top: 5px;
  padding: 10px;
  background-color: #F3F6FB;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border-radius: 8px;
`;

export const Flex = styled.View`
 display: flex;
 flex-direction: row;
 align-items: center
`;

export const Block = styled.View`
 display: flex;
 flex-direction: column;
 justify-content: center;
 align-items: flex-start;
`

export const Text = styled(TextComponent)`
  max-width: ${(props) => (props?.maxWidth || 320) - 150 }
  margin-left: 5px;
`;

export const IconButton = styled.TouchableOpacity`
  padding: 5px;
  opacity: ${props => props.opacity || '1'}
`
