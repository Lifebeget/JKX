import React, { memo, useCallback, useEffect, useState } from "react";
import { MainBlock, BlockItem, Flex, Text, Block, IconButton } from "./style";
import File from "../../../../assets/svg/file";
import { useSelector } from "react-redux";
import {
  fetchDeleteFile,
  fetchFilesQuestion,
  fetchToken,
} from "../../../../actions/fetch";
import Download from "../../../../assets/svg/download";
import Delete from "../../../../assets/svg/delete";
import { Platform, PermissionsAndroid } from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import { useToast } from "react-native-toast-notifications";

const FileBlock = ({ documentTypes, question_id }) => {
  const { width } = useSelector((state) => state.App.Dimensions);
  const [files, setFiles] = useState([]);
  const toast = useToast();

  useEffect(() => {
    fetchFilesQuestion(question_id).then((response) => {
      if (response instanceof Array && response.length) {
        setFiles([...response]);
      }
    });
  }, [question_id, documentTypes]);

  const checkPermission = async (token, url, file) => {
    if (Platform.OS === "ios") {
      downloadFile(token, url, file);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Требуется разрешение на хранение",
            message:
              "Приложению требуется доступ к вашему хранилищу для загрузки файла",
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(token, url, file);
        } else {
          toast.show("Разрешение на хранение не предоставлено", {
            type: "danger",
            placement: "bottom",
            duration: 4000,
            offset: 30,
            animationType: "slide-in",
          });
        }
      } catch (err) {
        console.log("++++" + err);
      }
    }
  };

  const downloadFile = (token, url, file) => {
    const fileUrl = `${url}api/uploads/${file.upload_id}/`;
    let FILE_URL = fileUrl;
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path: `${RootDir}/${file.original_file_name}`,
        description: "скачивание файла...",
        notification: true,
        useDownloadManager: true,
      },
    };
    config(options)
      .fetch("GET", FILE_URL, {
        Authorization: `Bearer ${token}`,
      })
      .then((res) => {
        toast.show("Файл успешно загружен.", {
          type: "success",
          placement: "bottom",
          duration: 4000,
          offset: 30,
          animationType: "slide-in",
        });
      });
  };

  const onDownload = (file) => {
    return fetchToken().then(({ token, baseUrl }) => {
      if (token) {
        checkPermission(token, baseUrl, file);
      }
    });
  };

  const onDelete = (upload_id, index) => {
    return fetchDeleteFile(upload_id)
      .then((message) => {
        let file = [...files];
        file.splice(index, 1);
        setFiles([...file]);
        toast.show(message, {
            type: "success",
            placement: "bottom",
            duration: 4000,
            offset: 30,
            animationType: "slide-in",
          });
      })
      .catch((e) => {
        console.warn(e.message);
      });
  };

  return (
    <MainBlock>
      {files.map((item, index) => {
        return (
          <BlockItem key={index}>
            <Flex>
              <File />
              <Block>
                <Text fontSize={10} maxWidth={width} color={"#464141"}>
                  {documentTypes[item?.document_type_id]?.document_types_name ||
                    ""}
                </Text>
                <Text fontSize={10} maxWidth={width} color={"#464141"}>
                  {item?.original_file_name}
                </Text>
              </Block>
            </Flex>
            <Flex>
              <IconButton onPress={() => onDownload(item)}>
                <Download />
              </IconButton>
              <IconButton opacity="0.4" disabled onPress={() => onDelete(item.upload_id, index)}>
                <Delete />
              </IconButton>
            </Flex>
          </BlockItem>
        );
      })}
    </MainBlock>
  );
};

export default memo(FileBlock);
