import React, { memo } from "react";
import { MainBlock, Text } from "./style";

const NoExistPolls = () => {
  return (
    <MainBlock>
      <Text color="#1F2021" fontSize={10}>
        Нет голосований
      </Text>
    </MainBlock>
  );
};

export default memo(NoExistPolls);
