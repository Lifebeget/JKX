import styled from 'styled-components/native';
import TextComponent from '../../../../components/Text';

export const MainBlock = styled.View`
  width: 100%;
  border-bottom-width: 1px;
  border-bottom-color: #eee;
  display: flex;
  padding: 15px;
  flex-direction: row
`;


export const Text = styled(TextComponent)``