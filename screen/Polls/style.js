import styled from 'styled-components/native';
import theme from '../../assets/theme';
import TextComponent from '../../components/Text';

export const MainView = styled.View`
  width: 100%;
  height: 100%;
  flex: 1;
  background-color: ${theme.white};
`;

export const HeaderBlock = styled.View`
 display: flex;
 width: 100%;
`;

export const BlockHeader = styled.View`
 display: flex;
 flex-direction: row;
 padding: 15px;
 justify-content: space-between
`;

export const HeaderTextBlock = styled.View`
 display: flex;
 flex-direction: column;
 max-width: 160px
`;

export const Text = styled(TextComponent)``;

export const HeaderRightBlock = styled.View`
 max-width: 150px;
 display: flex;
 align-items: flex-end;
`