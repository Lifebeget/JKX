import moment from "moment";

export const dateConverter = (date) => {
    return new Date(moment(date).unix() * 1000);
};

export class pollsValueConverter {
    toView(polls, owner_id, obj) {
      let result = [];
      if (polls) {
        for (let poll_id in polls) {
          let poll = polls[poll_id];
          const objPoll = obj[poll?.poll_id?.toString()] || {};
          let $current = new Date(),
            $start_date = new Date(dateConverter(poll?.start_date)),
            $end_date = new Date(dateConverter(poll.end_date));
          
          let item = {
            obj: {...objPoll,...poll},
            start_date: poll?.start_date,
            end_date: poll.end_date,
            house_id: poll.house_id
          };
  
          if (!poll.question.length) {
            continue;
          }
  
          if ($current < $start_date) {
            item.type = 'future';
            item.collapse = true;
          }
          if ($current > $end_date) {
            item.type = 'finish';
            item.collapse = true;
            item.info = 'Важная информация о прошедшем голосовании';
  
            // check total area
            let house_area = poll.total_area;
            let total = 0;
  
            let all_owners = {};
  
            // check your vote
            for (let i = 0; i < poll.question.length; i++) {
              let question = poll.question[i];
              question.yourVote = 'ignore';
  
              if (!question.vote) {
                continue;
              }
  
              for (let j = 0; j < question.vote.length; j++) {
                let vote = question.vote[j];
  
                if (vote.owner_id == owner_id) {
                  if (vote.vote > 0) {
                    question.yourVote = 'like'
                  }
                  if (vote.vote < 0) {
                    question.yourVote = 'dislike'
                  }
                }
  
                if (!all_owners[vote.owner_id]) {
                  all_owners[vote.owner_id] = true;
                  total += +poll.ownerMap[vote.owner_id].area;
                }
              }
            }
            // check Poll Quorum
            let poll_quorum = total / house_area * 100;
            if (poll_quorum < 50) {
              poll.noQuorum = true;
            }
  
            // results
            for (let i = 0; i < poll.question.length; i++) {
              let question = poll.question[i];
  
              let result = {
                likes: 0,
                dislikes: 0,
                ignore: 100.00,
                status: '',
              };
  
              let quorums = {
                1: 50,
                2: (2 / 3 * 100).toFixed()
              };
  
  
              if (question.vote) {
                for (let j = 0; j < question.vote.length; j++) {
                  let vote = question.vote[j];
                  let area = +poll.ownerMap[vote.owner_id].area;
                  let percent = total ? (area / total * 100) : 0;
  
                  if (vote.vote > 0) {
                    result.likes += percent;
                  } else if (vote.vote < 0) {
                    result.dislikes += percent;
                  }
                }
              }
  
              result.likes = +result.likes.toFixed(2);
              result.dislikes = +result.dislikes.toFixed(2);
              result.ignore = 100 - (result.likes + result.dislikes);
              result.ignore = +result.ignore.toFixed(2);
  
              let quorum = false;
  
              if (quorums[question.type]) {
                quorum = +quorums[question.type];
              }
  
              result.status = 'принят';
              question.success = true;
              console.log(poll.noQuorum)
              if (poll.noQuorum) {
                result.status = 'кворум не собран';
                question.ignored = true;
                question.success = false;
              } else {
                if (quorum) {
                  if (result.likes < quorum) {
                    result.status = 'кворум не собран';
                    question.fail = true;
                    question.success = false;
                  }
                } else {
                  if (result.likes < result.dislikes) {
                    result.status = 'не принят';
                    question.fail = true;
                    question.success = false;
                  }
                }
              }
              question.result = result;
            }
          }
          if ($current >= $start_date && $current <= $end_date) {
            item.type = 'active';
          }
          result.push(item);
        }
      }
      return [...result].sort((a, b) =>{
        return dateConverter(b.start_date) - dateConverter(a.start_date)
      });
    }
  }