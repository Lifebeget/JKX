import React, { useCallback, useEffect, useMemo, useState } from "react";
import theme from "../../assets/theme";
import HeadNav from "../../components/Header";
import {
  MainView,
  HeaderBlock,
  BlockHeader,
  HeaderTextBlock,
  Text,
  HeaderRightBlock,
} from "./style";
import PoolsTab from "./components/PollsTab";
import { useSelector } from "react-redux";
import {
  fetchDocumentTypes,
  fetchHouses,
  fetchOwners,
  fetchPollResults,
  fetchPolls,
  fetchQuestion,
  fetchVotes,
} from "../../actions/fetch";
import { useLogout } from "../../actions/hooks/logout";
import { CONSTANTS } from "../../actions/constants";
import { dateConverter, pollsValueConverter } from "./converter";

const pollsConverter = new pollsValueConverter();

const Polls = (props) => {
  const activeRoom = useSelector((state) => state.Main.activeRoom);
  const rooms = useSelector((state) => state.Main.rooms);
  const ownerInfo = useSelector((state) => state.Login.ownerInfo);
  const logout = useLogout();
  const [polls, setPolls] = useState([]);
  const [loading, setLoading] = useState(true);
  const [all_houses, setAllHouses] = useState({});
  const [ownerships, setOwnerships] = useState({});
  const [documentTypes, setDocumentTypes] = useState([]);

  const house_id = useMemo(() => {
    let id = null;
    try {
      id = rooms[activeRoom]?.house?.house_id;
    } catch (e) {
      console.warn(e.message, "error message");
    }
    return id;
  }, [rooms, activeRoom]);

  const total_area = useMemo(() => {
    let total = 0;
    try {
      total = all_houses[house_id].total_area;
    } catch (e) {
      console.warn(e.message, "error message");
    }
    return parseFloat(total).toFixed(2);
  }, [all_houses]);

  const sumArea = useMemo(() => {
    let your_area = 0;
    for (let room_id in rooms) {
      try {
        let room = rooms[room_id];
        let owner_percent = +(ownerships[room_id].percent || 100) / 100;
        if (room.house.house_id == house_id) {
          your_area += +room.total_area * owner_percent;
        }
      } catch (e) {
        console.warn(e.message, "error message");
      }
    }
    return parseFloat(your_area).toFixed(2);
  }, [total_area, ownerships]);

  const percent = useMemo(() => {
    return parseFloat(((+sumArea || 0) / (+total_area || 0)) * 100).toFixed(2);
  }, [sumArea]);

  const request = () => {
    setLoading(true);
    return fetchPolls(house_id)
      .then((response) => {
        if (!response || !response.length || !response instanceof Array) {
          throw new Error();
        }
        return response;
      })
      .then((pollsList) => {
        let promises = [];
        let pollObj = {};
        for (let key in [...pollsList]) {
          let poll = { ...pollsList[key] };
          pollObj[poll.poll_id] = poll;
          promises.push(
            fetchQuestion(poll.poll_id)
              .then((res) => {
                let _poll = { ...res };
                poll.question = _poll.question || [];
                poll.question.sort((a, b) => (+a.number > +b.number ? 1 : -1));
                return { ...res, ...poll };
              })
              .catch((e) => {
                console.warn(e.message, "error message");
                setLoading(false);
                if (
                  e?.message?.trim() === CONSTANTS.invalid_token_message.trim()
                ) {
                  logout();
                }
              })
          );
        }
        return Promise.all(promises).then((responses) => {
          const pollList = [...responses];
          return { pollList, pollObj };
        });
      })
      .then(({ pollList, pollObj }) => {
        let promises = [];
        let total_area = 0;
        for (let poll_id in pollList) {
          let poll = pollList[poll_id];
          console.log(poll.start_date)
          let $current = dateConverter(new Date()),
            $start_date = dateConverter(poll?.start_date),
            $end_date = dateConverter(poll.end_date);
          pollList[poll_id].end_date = $end_date;
          pollList[poll_id].start_date = $start_date;
          if ($current < $start_date) {
            pollList[poll_id].type = "future";
            pollList[poll_id].collapse = true;
          }
          if ($current > $end_date) {
            pollList[poll_id].type = "finish";
            pollList[poll_id].collapse = true;
            pollList[poll_id].info =
              "Важная информация о прошедшем голосовании";
          }
          if ($current > $end_date) {
            promises.push(
              new Promise(resolve =>{
                fetchPollResults(pollList[poll_id].poll_id)
                .then((response) => {
                  let _poll = response;
                  let owners = _poll.owner;
                  let questions = _poll.question;
                  if (all_houses[_poll.house_id]) {
                    total_area =
                      _poll?.total_area ||
                      all_houses[_poll.house_id]?.total_area;
                  }
                  poll.total_area = _poll.total_area;
                  poll.finish = true;
                  poll.poll_type = 0;
                  poll.ownerMap = {};
                  for (let i = 0; i < owners.length; i++) {
                    try {
                      let owner = owners[i];
                      owner.area = +owner.area;
                      if (!poll.ownerMap[owner.owner_id]) {
                        poll.ownerMap[owner.owner_id] = owner;
                      } else {
                        poll.ownerMap[owner.owner_id].area += owner.area;
                      }
                    } catch (e) {
                      console.warn(e.message, "error message");
                    }
                  }
                  for (let i = 0; i < questions.length; i++) {
                    try {
                      let _question = questions[i];
                      if (_question.vote) {
                        for (let j = 0; j < poll.question.length; j++) {
                          let question = poll.question[j];
                          if (question.question_id == _question.question_id) {
                            question.vote = [];
                            for (let k = 0; k < _question.vote.length; k++) {
                              let vote = _question.vote[k];
                              if (poll.ownerMap[vote.owner_id]) {
                                question.vote.push(vote);
                              }
                            }
                          }
                        }
                      }
                    } catch (e) {
                      console.warn(e.message, "error message");
                    }
                  }
                  resolve(poll)
                  return poll;
                })
                .catch((e) => {
                  console.warn(e.message, "error message");
                  resolve(poll)
                  if (
                    e?.message?.trim() ===
                    CONSTANTS.invalid_token_message.trim()
                  ) {
                    logout();
                  }
                })
              })
            );
          } else if ($current >= $start_date && $current <= $end_date) {
            promises.push(
              fetchVotes(pollList[poll_id].poll_id)
                .then((response) => {
                  let _poll = response;
                  let questions = _poll.question;
                  for (let i = 0; i < questions.length; i++) {
                    let _question = questions[i];
                    for (let j = 0; j < poll.question.length; j++) {
                      let question = poll.question[j];
                      if (question.question_id == _question.question_id) {
                        question.vote = _question.vote;
                      }
                    }
                  }
                  _poll.poll_type = 1;
                  _poll.start_date = $start_date;
                  _poll.end_date = $end_date;
                  return _poll;
                })
                .catch((e) => {
                  console.warn(e.message, "error message");
                  setLoading(false);
                  if (
                    e?.message?.trim() ===
                    CONSTANTS.invalid_token_message.trim()
                  ) {
                    logout();
                  }
                })
            );
          } else {
            if ($current >= $start_date && $current <= $end_date) {
              poll.type = "active";
            }
            promises.push(
              Promise.resolve({
                ...(poll || {}),
                poll_type: 2,
                start_date: $start_date,
                end_date: $end_date,
              })
            );
          }
        }
        return Promise.all(promises).then((responses) => {
          const pollsList = pollsConverter.toView(
            responses,
            ownerInfo.owner_id,
            pollObj
          );
          let houses = { ...all_houses };
          if (houses[house_id] && total_area) {
            houses[house_id].total_area =
              total_area || houses[house_id].total_area;
            setAllHouses({ ...houses });
          }

          setPolls([...pollsList]);
          setLoading(false);
        });
      })
      .catch((e) => {
        console.warn(e.message, "error message");
        setLoading(false);
        if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
          logout();
        }
      });
  };

  useEffect(() => {
    fetchDocumentTypes()
      .then((response) => {
        if (response.length) {
          setDocumentTypes([...response]);
        }
      })
      .then(() => {
        fetchHouses()
          .then((response) => {
            if (response && response.length && response instanceof Array) {
              let obj = {};
              for (let house of response) {
                try {
                  house.total_area = +house.total_area || 0;
                  obj[house.house_id] = { ...house };
                } catch (e) {
                  console.warn(e.message, "error message");
                }
              }
              setAllHouses({ ...obj });
            }
            fetchOwners()
              .then((response) => {
                if (
                  response &&
                  response instanceof Object &&
                  response.ownerships
                ) {
                  if (
                    response.ownerships.length &&
                    response.ownerships instanceof Array
                  ) {
                    let objOwn = {};
                    let ownerships = response.ownerships;
                    for (let i = 0; i < ownerships.length; i++) {
                      let ownership = ownerships[i];
                      objOwn[ownership.room_id] = ownership;
                    }
                    setOwnerships({ ...objOwn });
                  }
                }
                request();
              })
              .catch((e) => {
                console.warn(e.message, "error message");
                if (
                  e?.message?.trim() === CONSTANTS.invalid_token_message.trim()
                ) {
                  logout();
                  return;
                }
                request();
              });
          })
          .catch((e) => {
            console.warn(e.message, "error message");
            if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
              logout();
              return;
            }
            request();
          });
      })
      .catch((e) => {
        console.warn(e.message, "error message");
        if (e?.message?.trim() === CONSTANTS.invalid_token_message.trim()) {
          logout();
          return;
        }
        request();
      });
  }, [house_id]);

  const onRefresh = useCallback(() => {
    return request();
  }, [house_id]);

  return (
    <MainView>
      <HeadNav
        title="Голосования"
        menu
        onPress={() => {
          props.navigation.toggleDrawer();
        }}
      />
      <HeaderBlock>
        <BlockHeader>
          <HeaderTextBlock>
            <Text color={theme.black} fontSize={10}>
              Площадь помещений собственника:{" "}
              <Text color={theme.black} fontSize={11} fontWeight={"700"}>
                {loading ? '0.00' : sumArea || '0.00'} м2
              </Text>
            </Text>
            <Text color={theme.black} fontSize={10}>
              Площадь дома:
              <Text color={theme.black} fontSize={11} fontWeight={"700"}>
                {loading ? '0.00' : total_area || '0.00'} м2
              </Text>
            </Text>
          </HeaderTextBlock>
          <HeaderRightBlock>
            <Text color={theme.black} fontSize={10}>
              Вес вашего голоса
            </Text>
            <Text color={theme.green} fontSize={11}>
             {loading ? '0.00' : percent || '0.00'}%
            </Text>
          </HeaderRightBlock>
        </BlockHeader>
      </HeaderBlock>
      <PoolsTab
        documentTypes={documentTypes}
        polls={polls}
        loading={loading}
        onRefresh={onRefresh}
      />
    </MainView>
  );
};

export default Polls;
